from django.urls import path

from account.api.check_account import CheckAccountAPI
from account.api.create_account import CreateAccount
from account.api.deposit_money import DepositMoneyAPI
from account.api.transfer_money import TransferMoneyAPI

app_name = 'account'

urlpatterns = [
       path('', CreateAccount.as_view()),
       path('deposit', DepositMoneyAPI.as_view()),
       path('transfer', TransferMoneyAPI.as_view()),
       path('<str:account_guid>', CheckAccountAPI.as_view()),
]
