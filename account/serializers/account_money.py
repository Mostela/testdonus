from rest_framework import serializers

from account.models.account_user import AccountMoney
from account.serializers.account_user import AccountUserSerializer


class AccountMoneySerializer(serializers.ModelSerializer):
    balance_format = serializers.SerializerMethodField()
    user = AccountUserSerializer()

    def get_balance_format(self, obj):
        return f"R$ {obj.balance / 100}"

    class Meta:
        model = AccountMoney
        fields = ('number', 'balance', 'balance_format', 'user')
