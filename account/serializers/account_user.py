from rest_framework import serializers

from account.models.account_user import AccountUser


class AccountUserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('full_name', 'cpf', 'guid')
        model = AccountUser
