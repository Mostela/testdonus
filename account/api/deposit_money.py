from rest_framework.response import Response
from rest_framework.views import APIView

from account.service.money_deposit import MoneyDepositService


class DepositMoneyAPI(APIView):
    def post(self, request):
        service = MoneyDepositService()
        response = service.execute(request.data)
        return Response(response.message, status=response.status)
