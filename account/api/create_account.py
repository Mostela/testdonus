from rest_framework.response import Response
from rest_framework.views import APIView

from account.service.new_account import NewAccountService


class CreateAccount(APIView):
    def post(self, request):
        service = NewAccountService()
        response = service.execute(request.data)
        return Response(response.message, status=response.status)
