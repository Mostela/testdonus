from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from account.service.money_tranfer import MoneyTransferService


class TransferMoneyAPI(APIView):
    def post(self, request):
        service = MoneyTransferService()
        response = service.execute(request.data)
        return Response(response.message, status=response.status)
