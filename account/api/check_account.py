from rest_framework.response import Response
from rest_framework.views import APIView

from account.service.check_account import CheckAccountService


class CheckAccountAPI(APIView):
    def get(self, request, account_guid):
        service = CheckAccountService()
        response = service.execute({'number': account_guid})
        return Response(response.message, status=response.status)
