import re

from account.models.account_user import AccountUser, AccountMoney
from interfaces.response_service import ResponseService
from interfaces.service_abstract import ServiceAbstract


class MoneyOperationsServiceABS(ServiceAbstract):

    def return_account(self, number: str) -> AccountMoney:
        try:
            return AccountMoney.objects.get(number=number)
        except AccountMoney.DoesNotExist:
            return None

    def clean_value_balance(self, balance: str) -> int:
        return int(re.sub(r"[^\d]", "", balance))

    def execute(self, data: dict) -> ResponseService:
        pass
