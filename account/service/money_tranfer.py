from rest_framework import status

from account.serializers.account_money import AccountMoneySerializer
from account.serializers.account_user import AccountUserSerializer
from account.service.money_operations import MoneyOperationsServiceABS
from interfaces.response_service import ResponseService


class MoneyTransferService(MoneyOperationsServiceABS):
    def execute(self, data: dict) -> ResponseService:
        account_owner = self.return_account(data.get('account_owner'))
        account_client = self.return_account(data.get('account_client'))
        value = self.clean_value_balance(data.get('value'))
        if not account_owner or not account_client:
            return ResponseService("Operation can't be finish. Accounts not found", status=status.HTTP_400_BAD_REQUEST)

        if value >= account_owner.balance:
            return ResponseService("Operation can't be finish. Account not found money",
                                   status=status.HTTP_400_BAD_REQUEST)

        account_client.balance = account_client.balance + value
        account_client.save()

        account_owner.balance = account_owner.balance - value
        account_owner.save()

        return ResponseService(AccountMoneySerializer(instance=account_owner).data, status.HTTP_202_ACCEPTED)
