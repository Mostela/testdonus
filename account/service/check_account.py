from django.db.models import Q
from rest_framework import status

from account.models.account_user import AccountUser, AccountMoney
from account.serializers.account_money import AccountMoneySerializer
from account.serializers.account_user import AccountUserSerializer
from interfaces.response_service import ResponseService
from interfaces.service_abstract import ServiceAbstract


class CheckAccountService(ServiceAbstract):
    def execute(self, data: dict) -> ResponseService:
        try:
            return ResponseService(AccountMoneySerializer(AccountMoney.objects.get(number=data.get('number'))).data,
                                   status=status.HTTP_200_OK)
        except AccountUser.DoesNotExist:
            return ResponseService("account with this number not exist", status=status.HTTP_400_BAD_REQUEST)
