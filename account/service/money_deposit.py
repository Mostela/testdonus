from rest_framework import status

from account.serializers.account_money import AccountMoneySerializer
from account.service.money_operations import MoneyOperationsServiceABS
from interfaces.response_service import ResponseService


class MoneyDepositService(MoneyOperationsServiceABS):
    def execute(self, data: dict) -> ResponseService:
        account_owner = self.return_account(data.get("account_owner"))

        if not account_owner:
            return ResponseService("Not found account", status=status.HTTP_400_BAD_REQUEST)
        value = self.clean_value_balance(data.get('value'))

        if value > 200000:
            return ResponseService("this value is above limit R$ 2,000", status=status.HTTP_400_BAD_REQUEST)

        if value <= 0:
            return ResponseService("this value is zero", status=status.HTTP_400_BAD_REQUEST)

        account_owner.balance = account_owner.balance + value
        account_owner.save()
        return ResponseService(AccountMoneySerializer(instance=account_owner).data, status=status.HTTP_202_ACCEPTED)


