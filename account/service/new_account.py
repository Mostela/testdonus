import datetime
import random
import re
import uuid

from rest_framework import status

from account.models.account_user import AccountUser, AccountMoney
from account.serializers.account_money import AccountMoneySerializer
from account.serializers.account_user import AccountUserSerializer
from interfaces.response_service import ResponseService
from interfaces.service_abstract import ServiceAbstract


class NewAccountService(ServiceAbstract):
    def clean_cpf(self, cpf: str):
        return re.sub(r"[^\d]", "", cpf)

    def generate_number_account(self) -> int:
        # Think have a rule to generate account number not a randon number
        account_number = random.randint(0, 1000)
        while AccountMoney.objects.filter(number=account_number).exists():
            account_number = random.randint(0, 1000)
        return account_number

    def create_account(self, user: AccountUser) -> AccountMoney:
        money = AccountMoney()
        money.user = user
        money.balance = 0
        money.number = self.generate_number_account()
        money.save()
        return money

    def execute(self, data: dict) -> ResponseService:
        data['cpf'] = self.clean_cpf(data.get('cpf'))
        data['guid'] = uuid.uuid4().hex
        serializer = AccountUserSerializer(data=data)
        if serializer.is_valid():
            user = serializer.save()
            account_money = self.create_account(user)
            return ResponseService(AccountMoneySerializer(account_money).data, status=status.HTTP_201_CREATED)
        return ResponseService(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
