from django.db import models


class AccountUser(models.Model):
    full_name = models.TextField(blank=False, null=False)
    cpf = models.CharField(max_length=11, null=False, blank=False, db_index=True, unique=True)
    guid = models.UUIDField(blank=False, null=False, db_index=True)

    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class AccountMoney(models.Model):
    user = models.ForeignKey(AccountUser, on_delete=models.CASCADE, null=False, blank=False)

    number = models.CharField(max_length=4, null=False, blank=False, default=0)
    balance = models.IntegerField(null=False, blank=False, default=0)

    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
