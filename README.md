# Teste Donus - Backend - Python

### Autor: Joao Carlos Camargo

---

Para o teste foi utilizado o framework Python Django.

Os valores nao estao em forma de valores com float (ponto flutuante) apenas na forma de inteiro sendo assim
R$ 100,00 se escreve 10000 apenas removendo a virgula facilitando a manutençao futura e conversao para outros valores

## Iniciando o projeto

1. Tenha Python +3.9 instalado
2. pip3 install -r requeriments.txt
3. Salve as variaveis de ambiente presente na sua maquina antes de seguir com os proximos passos

## Executando o projeto

1. Certifique-se que tenha instalado em sua maquina o Docker
2. Tenha livre a porta 8000 da maquina a qual ira executar o projeto
3. Rode o comando abaixo em um terminal
    docker-compose up --build -d
4. Aguarde ato a montagem da imagem e a subida do servidor
5. Acesse o container para o nosso cenario de testes
6. _docker exec -it donus-server /bin/bash_
7. execute o comando _python3 manage.py migrate_
8. **O Servidor esta pronto para receber requisicao na porta 8000 do seu computador**
9. Pode utilizar o arquivo insominia.json localizado na raiz do diretorio para realizar as chamadas
   
## Executando testes
1. Rode os comandos do django cli em seu terminal ou dentro do container _docker exec -it donus-server /bin/bash_
2. 
   1. python3 manage.py migrate
   2. python3 manage.py test
3. Aguarde para o final dos testes unitarios