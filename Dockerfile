FROM python:3.6
WORKDIR donus
COPY . .
RUN pip install -r requirements.txt
RUN pip install gunicorn
EXPOSE 8000
ENTRYPOINT gunicorn testdonus.wsgi -b 0.0.0.0:8000