import abc
import logging

from interfaces.response_service import ResponseService


class ServiceAbstract(metaclass=abc.ABCMeta):
    def __init__(self):
        # TODO: Configurar o log
        self.log = logging

    @abc.abstractmethod
    def execute(self, data: dict) -> ResponseService:
        pass
