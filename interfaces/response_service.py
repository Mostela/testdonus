from dataclasses import dataclass
from rest_framework import status as status_http


@dataclass
class ResponseService:
    status: status_http
    message: dict

    def __init__(self, message, status):
        self.message = message
        self.status = status

