import unittest

from rest_framework import status
from django.test import TestCase

from account.service.check_account import CheckAccountService
from account.service.new_account import NewAccountService

example_data = {
    "full_name": "Joao Carlos Camargo Andrade",
    "cpf": "48861730809"
}


class CreateAccountTestCase(TestCase):
    def test_new_account(self):
        service = NewAccountService()
        data_response = service.execute(example_data)
        self.assertEqual(data_response.status, status.HTTP_201_CREATED)
        user_data = data_response.message.get('user')
        self.assertEqual(user_data.get('cpf'), example_data.get('cpf'))
        self.assertEqual(user_data.get('full_name'), example_data.get('full_name'))
        self.assertIsNotNone(user_data.get('guid'))
        self.assertIsNotNone(data_response.message.get('number'))
        self.assertEqual(type(data_response.message.get('number')), str)
        self.assertIsNotNone(data_response.message.get('balance'))
        self.assertEqual(type(data_response.message.get('balance')), int)

    def test_cpf_clean(self):
        service = NewAccountService()
        cpf_example = '488-617-308-09'
        self.assertIsNotNone(service.clean_cpf(cpf_example))
        self.assertRegex(service.clean_cpf(cpf_example), "^[0-9]*$")

    def test_number_account(self):
        service = NewAccountService()
        self.assertIsNotNone(service.generate_number_account())
        self.assertEqual(type(service.generate_number_account()), int)
