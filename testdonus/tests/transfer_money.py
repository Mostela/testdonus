import unittest
from django.test import TestCase
from rest_framework import status

from account.service.money_deposit import MoneyDepositService
from account.service.money_tranfer import MoneyTransferService
from account.service.new_account import NewAccountService


class TransferMonetTesCase(TestCase):
    def create_account(self, data: dict):
        service = NewAccountService()
        return service.execute(data)

    def create_account_owner(self):
        example_data = {
            "full_name": "Joao Carlos Andrade",
            "cpf": "48861730000"
        }
        return self.create_account(example_data)

    def create_account_client(self):
        example_data = {
            "full_name": "Joao Camargo",
            "cpf": "48861730001"
        }
        return self.create_account(example_data)

    def test_add_money(self):
        service = MoneyDepositService()
        owner = self.create_account_owner()
        data = {
            "account_owner": owner.message.get('number'),
            "value": "10000"
        }
        response = service.execute(data)
        self.assertEqual(response.status, status.HTTP_202_ACCEPTED)
        self.assertEqual(int(response.message.get('balance')), int(data.get('value')))
        self.assertEqual(response.message.get('number'), data.get('account_owner'))
        return response

    def test_add_money_limit(self):
        service = MoneyDepositService()
        owner = self.create_account_owner()
        data = {
            "account_owner": owner.message.get('number'),
            "value": "200000"
        }
        response = service.execute(data)
        self.assertEqual(response.status, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.message, "this value is above limit R$ 2,000")

    def test_add_money_zero(self):
        service = MoneyDepositService()
        owner = self.create_account_owner()
        data = {
            "account_owner": owner.message.get('number'),
            "value": "0"
        }
        response = service.execute(data)
        self.assertEqual(response.status, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.message, "this value is zero")

    def test_account_not_found(self):
        service = MoneyDepositService()
        owner = self.create_account_owner()
        data = {
            "account_owner": int(owner.message.get('number')) + 1,
            "value": "1000"
        }
        response = service.execute(data)
        self.assertEqual(response.status, status.HTTP_400_BAD_REQUEST)

    def test_transfer_money(self):
        client_user = self.create_account_client().message.get('number')
        owner = self.test_add_money()
        start_money = int(owner.message.get('balance'))

        service = MoneyTransferService()
        data_example = {
            "account_owner": owner.message.get('number'),
            "account_client": client_user,
            "value": "1000"
        }

        response = service.execute(data_example)
        self.assertEqual(response.status, status.HTTP_202_ACCEPTED)
        self.assertGreater(start_money, 0)
        self.assertEqual(int(response.message.get('balance')), start_money - int(data_example.get('value')))
        self.assertGreaterEqual(int(response.message.get('balance')), 0)

    def test_transfer_money_no_funds(self):
        client_user = self.create_account_client().message.get('number')
        owner = self.test_add_money()
        start_money = int(owner.message.get('balance'))

        service = MoneyTransferService()
        data_example = {
            "account_owner": owner.message.get('number'),
            "account_client": client_user,
            "value": "1000000"
        }

        response = service.execute(data_example)
        self.assertEqual(response.status, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.message,  "Operation can't be finish. Account not found money")
        self.assertGreater(start_money, 0)
        self.assertLessEqual(start_money - int(data_example.get('value')), -1)

    def test_transfer_money_no_client(self):
        owner = self.test_add_money()
        start_money = int(owner.message.get('balance'))

        service = MoneyTransferService()
        data_example = {
            "account_owner": owner.message.get('number'),
            "account_client": "1",
            "value": "100"
        }

        response = service.execute(data_example)
        self.assertEqual(response.status, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.message,  "Operation can't be finish. Accounts not found")